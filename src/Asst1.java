import java.nio.charset.StandardCharsets;

public class Asst1 {


    public String ReverseString(String stringToBeReverse) {
        String str2 = "";
        for (int i = stringToBeReverse.length() - 1; i >= 0; i--) {
            str2 += stringToBeReverse.charAt(i);
        }
        return str2;
    }

    public String PhraseToAcronym(String PhraseToAcronym) {
        for (int i = 0; i >= PhraseToAcronym.length() - 1; i++) {
            System.out.println(PhraseToAcronym.charAt(i));
            if ("".equals(PhraseToAcronym.charAt(i))) {
                char x = PhraseToAcronym.charAt(i++);
                System.out.println(x);
            }

        }
        return PhraseToAcronym;
    }

    public String Triangle(int a, int b, int c) {
        if (a == b && b == c)
            return "Equilateral Triangle";
        else if (a == b || b == c || c == a)
            return "Isosceles Triangle";
        else
            return "Scalene Triangle";

    }

    public int Scrabble(String Scrabble) {
        int count = 0;
        for (int i = 0; i <= Scrabble.length() - 1; i++) {

            if ('A' == Scrabble.charAt(i)) {
                count = count + 1;
            }
                else if ('B' == Scrabble.charAt(i)){
                    count = count + 3;
                }
            else if ('C' == Scrabble.charAt(i)){
                count = count + 3;
            }
            else if ('D' == Scrabble.charAt(i)){
                count = count + 2;
            }
            else if ('E' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('F' == Scrabble.charAt(i)){
                count = count + 4;
            }
            else if ('G' == Scrabble.charAt(i)){
                count = count + 2;
            }
            else if ('H' == Scrabble.charAt(i)){
                count = count + 4;
            }
            else if ('I' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('J' == Scrabble.charAt(i)){
                count = count + 8;
            }
            else if ('K' == Scrabble.charAt(i)){
                count = count + 5;
            }
            else if ('L' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('M' == Scrabble.charAt(i)){
                count = count + 3;
            }
            else if ('N' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('O' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('P' == Scrabble.charAt(i)){
                count = count + 3;
            }
            else if ('Q' == Scrabble.charAt(i)){
                count = count + 10;
            }
            else if ('R' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('S' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('T' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('U' == Scrabble.charAt(i)){
                count = count + 1;
            }
            else if ('V' == Scrabble.charAt(i)){
                count = count + 4;
            }
            else if ('W' == Scrabble.charAt(i)){
                count = count + 4;
            }
            else if ('X' == Scrabble.charAt(i)){
                count = count + 8;
            }
            else if ('Y' == Scrabble.charAt(i)){
                count = count + 4;
            }
            else if ('Z' == Scrabble.charAt(i)){
                count = count + 10;
            }


        }
        return count;

    }

    public String cleanPhoneNumber(String cleanPhoneNumber){

        cleanPhoneNumber = cleanPhoneNumber.replaceAll("\\s", "");
        cleanPhoneNumber = cleanPhoneNumber.replaceAll("[()]", "");
        cleanPhoneNumber = cleanPhoneNumber.replaceAll("[+1]", "");
        cleanPhoneNumber = cleanPhoneNumber.replaceAll("[.]", "");
        cleanPhoneNumber = cleanPhoneNumber.replaceAll("[-]", "");



        return cleanPhoneNumber;

    }

    public int nthPrimeNumber(int nthPrimeNumber){
        int num = 1, count = 0, i;
        while(count<nthPrimeNumber){
            num = num+1;
            for(i = 2; i<=num; i++){
                if(num%i == 0){
                    break;
                }
            }
            if(i == num){
                count = count+1;
            }
        }

        return num;
    }

    public int countOfOccurrences(String countOfOccurrences, String word){
        String a[] = countOfOccurrences.split(" ");
        int count =0;
        for(int i =0; i < a.length; i++){
            if(word.equals(a[i]))
                count++;
        }
        return count;
    }








}